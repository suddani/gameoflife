//#include <windows.h>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
class CCell
{
public:
    CCell(int wert, int color) : m_wert(wert), m_color(color)
    {}
    CCell(void) : m_wert(0), m_color(0)
    {}
    int m_wert;
    int m_color;
};
void fillmatrix(CCell* m, int n)
{
    for (int i=0;i<n;i++)
    {
        for (int a=0;a<n;a++)
        {
            m[i*n+a].m_wert = rand()%2;
            m[i*n+a].m_color = rand()%2;
        }
    }
}
/*void printmat(int* m, int n)
{
    for(int i=0;i<n;i++)
    {
        for(int a=0;a<n;a++)
        {
            if(m[i*n+a] != 0)
                printf("+");
            else
                printf(" ");
        }
        printf("\n");
    }
}*/

class neighbar
{
public:
    neighbar(void) : nachbarn(0), team(0){}
    int nachbarn;
    int team;
};

neighbar nachbarn(CCell* m, int n, int j, int i)
{
    int left, right, up, down, nachbar = 0;
    int teamA = 0;
    int teamB = 0;
    if (i-1 < 0)
        left = n-1;
    else
        left = i-1;

    if (i+1>=n)
        right = 0;
    else
        right = i+1;

    if (j-1<0)
        up = n-1;
    else
        up = j-1;

    if (j+1>=n)
        down = 0;
    else
        down = j+1;

    if (m[up*n+i].m_wert == 1)
    {
        if (m[up*n+i].m_color == 0)
            teamA+=1;
        else
            teamB+=1;
        nachbar+=1;
    }
    if (m[down*n+i].m_wert == 1)
    {
        if (m[down*n+i].m_color == 0)
            teamA+=1;
        else
            teamB+=1;
        nachbar+=1;
    }
    if (m[j*n+left].m_wert == 1)
    {
        if (m[j*n+left].m_color == 0)
            teamA+=1;
        else
            teamB+=1;
        nachbar+=1;
    }
    if (m[j*n+right].m_wert == 1)
    {
        if (m[j*n+right].m_color == 0)
            teamA+=1;
        else
            teamB+=1;
        nachbar+=1;
    }
    if (m[up*n+left].m_wert == 1)
    {
        if (m[up*n+left].m_color == 0)
            teamA+=1;
        else
            teamB+=1;
        nachbar+=1;
    }
    if (m[down*n+left].m_wert == 1)
    {
        if (m[down*n+left].m_color == 0)
            teamA+=1;
        else
            teamB+=1;
        nachbar+=1;
    }
    if (m[up*n+right].m_wert == 1)
    {
        if (m[up*n+right].m_color == 0)
            teamA+=1;
        else
            teamB+=1;
        nachbar+=1;
    }
    if (m[down*n+right].m_wert == 1)
    {
        if (m[down*n+right].m_color == 0)
            teamA+=1;
        else
            teamB+=1;
        nachbar+=1;
    }
    neighbar neu;
    neu.nachbarn = nachbar;
    if (teamA > teamB)
        neu.team = 0;
    else
        neu.team = 1;

    return neu;
}

void run(CCell* m, int n, int* teamA, int* teamB)
{
    *teamA = 0;
    *teamB = 0;
    CCell cpy[n*n];

    //memcpy(&cpy[0], &m[0], n*n);

    for (int i=0;i<n*n;i++)
    {
        cpy[i].m_wert=m[i].m_wert;
        cpy[i].m_color=m[i].m_color;
    }

    for (int i=0;i<n;i++)
    {
        for (int a=0;a<n;a++)
        {
            neighbar nbar = nachbarn(cpy, n, i,a);
            //printf("Nachbarn: %i\n", nbar.nachbarn);
            /*if (cpy[i*n+a].m_wert == 1 && nbar.nachbarn>=2 && nbar.nachbarn<=3)
                m[i*n+a].m_wert = 1;
            else if (cpy[i*n+a].m_wert == 0 && nbar.nachbarn==3)
                m[i*n+a].m_wert = 1;
            else
                m[i*n+a].m_wert = 0;*/
            if (cpy[i*n+a].m_wert == 1)
            {
                if (!(nbar.nachbarn>=2 && nbar.nachbarn<=3))
                    m[i*n+a].m_wert = 0;
            }
            else if (cpy[i*n+a].m_wert == 0)
            {
                if (nbar.nachbarn==3)
                {
                    m[i*n+a].m_wert = 1;
                    m[i*n+a].m_color = nbar.team;
                }
                else
                {
                    m[i*n+a].m_wert = 0;
                }
            }
            if (m[i*n+a].m_wert == 1)
            {
                if (m[i*n+a].m_color == 0)
                    *teamA += 1;
                else
                    *teamB += 1;
            }
        }
    }
}

void insertShape(CCell* mat, int size, int i, int j, CCell* matb, int n, int m)
{
    int a = i;
    int b = j;
    while (a>=size)
        a-=size;
    while (b>=size)
        b-=size;
    for (int s=0;s<n;s++)
    {
        if (a+s>=size)
            a-=size;
        for (int g=0;g<m;g++)
        {
            if (b+g>=size)
                b-=size;
            mat[(a+s)*size+(b+g)].m_wert = matb[s*n+g].m_wert;
            mat[(a+s)*size+(b+g)].m_color = matb[s*n+g].m_color;
        }
    }
}
#include <irrlicht.h>
using namespace irr;

void printmat(CCell* m, int n, video::IVideoDriver* driver)
{
    for (int i=0;i<n;i++)
    {
        for (int a=0;a<n;a++)
        {
            if (m[i*n+a].m_wert != 0)
            {
                //driver->draw2DPolygon(core::position2d< s32 >(a*10+5,i*10+5), 5, video::SColor(255, 255, 0, 0), 20);
                if (m[i*n+a].m_color == 0)
                    driver->draw2DRectangle(core::rect< s32 >(a*10,i*10,a*10+10,i*10+10), video::SColor(255,255,0,0), video::SColor(255,255,0,0), video::SColor(255,255,0,0), video::SColor(255,255,0,0), 0);
                else
                    driver->draw2DRectangle(core::rect< s32 >(a*10,i*10,a*10+10,i*10+10), video::SColor(255,0,255,0), video::SColor(255,0,255,0), video::SColor(255,0,255,0), video::SColor(255,0,255,0), 0);
            }
            else
            {
                /*driver->draw2DRectangle(core::rect< s32 >(a*10,i*10,a*10+10,i*10+10),
                                        video::SColor(255,0,255,255),
                                        video::SColor(255,0,255,255),
                                        video::SColor(255,0,255,255),
                                        video::SColor(255,0,255,255), 0);*/
                driver->draw2DRectangle(core::rect< s32 >(a*10,i*10,a*10+10,i*10+10),
                                        video::SColor(255,0,0,0),
                                        video::SColor(255,0,0,0),
                                        video::SColor(255,0,0,0),
                                        video::SColor(255,0,0,0), 0);
            }
        }
    }
}

class input : public IEventReceiver
{
public:
    input(CCell* mat, int n, bool* run, gui::IGUIEnvironment* gui, IrrlichtDevice* device, int* gen, int* teamA, int* teamB)
    {
        m_teamA = teamA;
        m_teamB = teamB;
        del = false;
        left_down = false;
        right_down = false;
        m_generation = gen;
        m_device = device;
        m_run = run;
        m_mat = mat;
        m_n = n;
        m_animate = gui->addButton(core::rect<s32>(650,50,750,80), NULL, 5, L"Start/Stop", L"click to start/stop animation");
        m_text = gui->addStaticText(L"Idle", core::rect<s32>(650,30,750,50));
        m_clear = gui->addButton(core::rect<s32>(650,90,750,120), NULL, 5, L"ClearWorld", L"click to clear the world");
        m_path = gui->addEditBox(L"world", core::rect<s32>(650,240,750,270));
        m_saveWorld = gui->addButton(core::rect<s32>(750,240,800,270), NULL, 5, L"SaveWorld", L"saves the current world");
        m_loadWorld = gui->addButton(core::rect<s32>(750,270,800,300), NULL, 5, L"LoadWorld", L"loads the worldfile");
        m_exit = gui->addButton(core::rect<s32>(650,130,750,160), NULL, 5, L"Fill field", L"click to fill the field");
        m_step = gui->addButton(core::rect<s32>(752,50,797,80), NULL, 5, L"Step", L"runs for just one generation");

    }
    void work(int X, int Y, bool del)
    {
        if (core::rect<s32>(0,0,600,600).isPointInside(core::position2d<s32>(X, Y)))
        {
            for (int i=0;i<60;i++)
            {
                for (int a=0;a<60;a++)
                {
                    if (core::rect<s32>(a*10,i*10,a*10+10,i*10+10).isPointInside(core::position2d<s32>(X, Y)))
                    {
                        if (left_down && !del)
                        {
                            m_mat[i*m_n+a].m_wert = 1;
                            m_mat[i*m_n+a].m_color = 0;

                        }
                        else if (right_down && !del)
                        {
                            m_mat[i*m_n+a].m_wert = 1;
                            m_mat[i*m_n+a].m_color = 1;
                        }
                        else if (left_down || right_down && del)
                        {
                            m_mat[i*m_n+a].m_wert = 0;
                        }
                    }
                }
            }
        }
    }
    virtual bool OnEvent (const SEvent &event)
    {
        switch (event.EventType)
        {
        case EET_KEY_INPUT_EVENT:
            if (event.KeyInput.Key == KEY_SHIFT)
                del = event.KeyInput.PressedDown;
            break;
        case EET_GUI_EVENT:
            if (event.GUIEvent.EventType == gui::EGET_BUTTON_CLICKED)
            {
                if (event.GUIEvent.Caller == m_animate)
                {
                    *m_run = !(*m_run);
                    if (*m_run)
                        m_text->setText(L"Running");
                    else
                        m_text->setText(L"Idle");
                    return true;
                }
                if (event.GUIEvent.Caller == m_step)
                {
                    run(&m_mat[0], m_n, m_teamA, m_teamB);
                    *m_generation += 1;
                    return true;
                }
                if (event.GUIEvent.Caller == m_saveWorld)
                {
                    core::stringc path = m_path->getText();
                    path.append(".GameOfLife");
                    FILE* datei;
                    datei = fopen(path.c_str(), "w+");
                    if (datei)
                    {
                        fwrite(&m_n, 4,1,datei);
                        fwrite(&m_mat[0].m_wert,8,m_n*m_n, datei);
                        fclose(datei);
                    }
                }
                if (event.GUIEvent.Caller == m_loadWorld)
                {
                    core::stringc path = m_path->getText();
                    path.append(".GameOfLife");
                    FILE* datei;
                    datei = fopen(path.c_str(), "r");
                    int mat_size = 0;
                    if (datei)
                    {
                        for (int i=0;i<m_n*m_n;i++)
                        {
                            m_mat[i].m_wert = 0;
                            m_mat[i].m_color = 0;
                        }
                        fread(&mat_size, 4,1,datei);
                        if (mat_size == m_n)
                            fread(&m_mat[0].m_wert,8,m_n*m_n, datei);
                        else
                        {
                            CCell* mymat = new CCell[mat_size*mat_size];
                            fread(&mymat[0].m_wert,8,mat_size*mat_size, datei);
                            insertShape(&m_mat[0], m_n, 0, 0, &mymat[0], mat_size, mat_size);
                            delete [] mymat;
                        }
                        fclose(datei);
                        *m_generation = 0;
                    }
                }
                if (event.GUIEvent.Caller == m_clear)
                {
                    for (int i=0;i<m_n*m_n;i++)
                    {
                        m_mat[i].m_wert = 0;
                        m_mat[i].m_color = 0;
                    }
                    *m_generation = 0;
                    return true;
                }
                if (event.GUIEvent.Caller == m_exit)
                {
                    fillmatrix(m_mat, m_n);
                    *m_generation = 0;
                    return true;
                }
            }
            break;
        case EET_MOUSE_INPUT_EVENT:
            switch (event.MouseInput.Event)
            {
            case EMIE_LMOUSE_PRESSED_DOWN:
                left_down = true;
                work(event.MouseInput.X, event.MouseInput.Y, del);
                return false;
                break;
            case EMIE_LMOUSE_LEFT_UP:
                left_down = false;
                return false;
                break;
            case EMIE_RMOUSE_PRESSED_DOWN:
                right_down = true;
                work(event.MouseInput.X, event.MouseInput.Y, del);
                return false;
                break;
            case EMIE_RMOUSE_LEFT_UP:
                right_down = false;
                return false;
                break;
            case EMIE_MOUSE_MOVED:
                if (left_down || right_down)
                    work(event.MouseInput.X, event.MouseInput.Y, del);
                return false;
                break;
            };
            break;
        };
        return false;
    }
protected:
    bool del;
    gui::IGUIEditBox* m_path;
    gui::IGUIElement* m_animate;
    gui::IGUIElement* m_clear;
    gui::IGUIElement* m_exit;
    gui::IGUIElement* m_step;
    gui::IGUIElement* m_saveWorld;
    gui::IGUIElement* m_loadWorld;
    gui::IGUIStaticText* m_text;
    IrrlichtDevice* m_device;
    bool* m_run;
    CCell* m_mat;
    int m_n;
    int* m_generation;
    int* m_teamA;
    int* m_teamB;
    bool left_down;
    bool right_down;
};

int main(int argc, char* argv[])
{
    const int n = 60;
    /*
    int shape[] = {0,0,0,0,0,0,0,0,0,
                   0,0,0,0,0,0,0,0,0,
                   0,0,0,0,1,0,0,0,0,
                   0,0,0,0,1,0,0,0,0,
                   0,0,1,1,1,1,1,0,0,
                   0,0,0,0,1,0,0,0,0,
                   0,0,0,0,1,0,0,0,0,
                   0,0,0,0,0,0,0,0,0,
                   0,0,0,0,0,0,0,0,0};
    int shape2[] = {0,0,1,1,0,
                    1,1,0,1,1,
                    1,1,1,1,0,
                    0,1,1,0,0,
                    0,0,0,0,0};*/
    /*int earth[n*n];/* = {0,0,0,0,0,0,0,0,0,
                   0,0,1,1,1,1,0,0,1,
                   0,0,0,0,0,1,0,0,1,
                   0,0,0,0,0,1,0,0,1,
                   0,0,1,1,1,1,1,1,1,
                   0,0,1,0,0,1,0,0,0,
                   0,0,1,0,0,1,0,0,0,
                   0,0,1,0,0,1,1,1,1,
                   0,0,0,0,0,0,0,0,0};*/
    CCell earth2[n*n];

    if (argc >= 2)
    {
        FILE* datei;
        datei = fopen(argv[1], "r");
        int mat_size = 0;
        if (datei)
        {

            fread(&mat_size, 4,1,datei);
            if (mat_size == n)
                fread(&earth2[0].m_wert,8,n*n, datei);
            else if (mat_size < n)
            {
                CCell* mymat = new CCell[mat_size*mat_size];
                fread(&mymat[0].m_wert,8,mat_size*mat_size, datei);
                insertShape(&earth2[0], n, 0, 0, &mymat[0], mat_size, mat_size);
                delete [] mymat;
            }
            fclose(datei);
        }
    }

    //fillmatrix(&earth[0], n);
    //insertShape(earth, n, 15, 15, shape2, 5,5);
    //insertShape(earth, n, 9, 15, shape2, 5,5);

    // start up the engine
    IrrlichtDevice *device = createDevice(video::EDT_OPENGL, core::dimension2d<u32>(800,600), 32, false, false, false, NULL);
    video::IVideoDriver* driver = device->getVideoDriver();
    scene::ISceneManager* scenemgr = device->getSceneManager();
    gui::IGUIEnvironment* gui = device->getGUIEnvironment();
    device->setWindowCaption(L"GameOfLife!");

    gui::IGUIStaticText* m_generation_text = gui->addStaticText(L"Generation: ", core::rect<s32>(650,160,750,190));
    gui::IGUIEditBox* m_time = gui->addEditBox(L"200", core::rect<s32>(650,190,750,210));
    gui::IGUICheckBox* m_draw = gui->addCheckBox(true, core::rect<s32>(752,190,798,210));

    gui::IGUIStaticText* m_stop_text = gui->addStaticText(L"Stop by generation: ", core::rect<s32>(745-80,190+25,745,210+35));
    gui::IGUICheckBox* m_stop = gui->addCheckBox(true, core::rect<s32>(752,190+15,798,210+25));
    //gui::IGUIEditBox* m_time_draw = gui->addEditBox(L"1", core::rect<s32>(650,210,750,230));
    int generation = 0;
    int teamA, teamB = 0;
    int teamA_last, teamB_last = 0;
    int constant = 0;

    bool animate = false;
    input i(&earth2[0], n, &animate, gui, device, &generation, &teamA, &teamB);
    device->setEventReceiver(&i);

    s32 last_time = 0;

    //s32 last_time_draw = 0;

    while (device->run())
    {
        if (last_time == 0)
            last_time = device->getTimer()->getTime();
        //if (last_time_draw == 0)
        //  last_time_draw = device->getTimer()->getTime();

        if (animate && device->getTimer()->getTime()-last_time>=atoi(core::stringc(m_time->getText()).c_str()))
        {
            last_time = device->getTimer()->getTime();
            run(&earth2[0], n, &teamA, &teamB);

            generation++;

            if (teamA == teamA_last && teamB == teamB_last && m_stop->isChecked())
            {
            	constant+=1;
            }
            else
				constant = 0;

			if (constant >= 5)
			{
				generation -= constant;
				animate = false;
				constant = 0;
			}

            teamA_last = teamA;
            teamB_last = teamB;
        }
        m_generation_text->setText((core::stringw(L"Generation: ")+core::stringw(generation)+core::stringw(L"\nTeamA: ")+core::stringw(teamA)+core::stringw(L" TeamB: ")+core::stringw(teamB)+core::stringw(L"\nFPS: ")+core::stringw(driver->getFPS())).c_str());

        driver->beginScene(true, true, video::SColor(255,0,0,255));
        if (m_draw->isChecked())
            printmat(&earth2[0], n, driver);
        gui->drawAll();
        driver->endScene();

        //Sleep(1);
    }
    device->drop();
    //system("cls");
    return 1;
}
